<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>


<%@page import="bank.Customer"%>
<%@page import="java.util.Iterator"%>

<jsp:useBean id="cust" scope="session" class="bank.Customer" />

<jsp:useBean id="custs" scope="session" class="java.util.ArrayList" />
<jsp:useBean id="error" scope="request" class="java.lang.String" />



<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<meta http-equiv="Expires" content="-1" />
<meta http-equiv="Pragma" content="no-cache" />
<meta http-equiv="Cache-Control" content="no-cache" />

<link rel="stylesheet" href="./css/print.css" type="text/css"
	media="print" />
<link rel="stylesheet" href="./css/screen.css" type="text/css" />
<title>Banque - Clients</title>
</head>
<body>
	<div id="bodyColumn">
		<a href="index.html">retour</a>
		<h1>Clients</h1>
		<div><%=error%></div>
		<span class="pageform">
			<form action="customers.do">
				<input name="page" type="hidden" value="${param.page}" /> <input
					name="id" type="hidden" value="${cust.id}" />
				<table border="0" cellspacing="1">
					<tr>
						<td class="label" width="100">id:</td>
						<td class="data" width="100">${cust.id}</td>
						<td class="label" width="100">nom:</td>
						<td class="data" width="100"><input name="name" type="text"
							value="${cust.name}" /></td>
						<td class="empty" width="350">&nbsp;</td>
					</tr>
					<tr>
						<td class="label" width="100">addresse:</td>
						<td class="data" width="100"><input name="address"
							type="text" value="${cust.address}" /></td>
						<td class="label" width="100">t�l�phone</td>
						<td class="data" width="100"><input name="phone" type="text"
							value="${cust.phone}" /></td>
						<td class="empty" width="350">&nbsp;</td>
					</tr>
					<tr>
						<td class="label" width="100">code postal:</td>
						<td class="data" width="100"><input name="zipCode"
							type="text" value="${cust.zipCode}" /></td>
						<td class="label" width="100">&nbsp;</td>
						<td class="data" width="100">&nbsp;</td>
						<td class="empty" width="350">&nbsp;</td>
					</tr>
				</table>

				<table border="0">
					<tr>
						<td width="100"><input name="cmd" type="submit"
							value="modifier" /></td>
						<td width="100"><input name="cmd" type="submit"
							value="ajouter" /></td>
					    <td width="100"><input name="cmd" type="submit"
							value="supprimer" /></td>
						<td width="100"><input name="cmd" type="submit"
							value="suivant" /></td>
						<td width="100"><input name="cmd" type="submit"
							value="pr�c�dent" /></td>
						<td width="100"><input name="cmd" type="submit"
							value="voir les comptes" /></td>
						<td width="50">&nbsp;</td>
					</tr>
				</table>
			</form>
		</span>
		<h3>Clients</h3>
		<table>
			<thead>
				<tr>
					<th>id</th>
					<th>&nbsp;</th>
					<th>nom</th>
					<th>addresse</th>
					<th>t�l�phone</th>
					<th>code postal</th>
				</tr>
			</thead>
			<tbody>
				<c:forEach items="${custs}" var="acust">
					<tr class="even">
						<c:if test="${acust.id==cust.id}">
							<td bgcolor="red"><a
								href="customers.do?cmd=editer&id=${acust.id}">${acust.id}</a></td>
						</c:if>
						<c:if test="${acust.id!=cust.id}">
							<td><a href="customers.do?cmd=editer&id=${acust.id}">${acust.id}</a>
							</td>
						</c:if>
						<td><a href="customers.do?cmd=supprimer&id=${acust.id}">supprimer</a></td>
						<td>${acust.name}</td>
						<td>${acust.address}</td>
						<td>${acust.phone}</td>
						<td>${acust.zipCode}</td>
					</tr>
				</c:forEach>
			</tbody>
		</table>
	</div>
</body>
</html>