package controler;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import bank.Customer;
import dao.CustomerDao;
import test.TestCustomer;
import utils.AppLocator;

/**
 * Servlet implementation class CustomerServlet
 */
@WebServlet("/customers.do")
public class CustomerServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	CustomerDao customerDao = null;

	/**
	 * @see Servlet#init(ServletConfig)
	 */
	public void init(ServletConfig config) throws ServletException {
		super.init(config);
		TestCustomer.main(null);
		customerDao = AppLocator.getInstance().getComponent("dao.CustomerDaoImpl");

	}

	private Customer formValue(HttpServletRequest request) {
		try {
			

		
		Customer result = new Customer();
		int cid = -1;
		try {
			cid = Integer.parseInt(request.getParameter("commune"));
		} catch (Exception e) {
			System.out.println("no cust city");
		}
		result.setId(Integer.parseInt(request.getParameter("id")));
		result.setName(request.getParameter("name"));
		// result.setForName(request.getParameter("forName"));
		result.setAddress(request.getParameter("address"));
		result.setZipCode(request.getParameter("zipCode"));
		/*
		try {
		Bank bank = session.getBankDao().find(Integer.parseInt(request.getParameter("bank")));
		result.setBank(bank);
		} catch (Exception e) {
			System.out.println("err cust bank");
		}
		

		if (cid!=-1){
			City city = session.getCityDao().find(cid);
		    result.setCity(city);
		}

	*/
		return result;
		} catch (Exception e) {
			return null;
		}
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		List<Customer> model = TestCustomer.mesCustomers;
		if (request.getSession().getAttribute(TestCustomer.CUSTOMER_LIST) == null)
			request.getSession().setAttribute(TestCustomer.CUSTOMER_LIST, TestCustomer.mesCustomers);

		Customer current = model.get(0);
		Customer newBean = null;
		Customer oldBean = null;

		String cmd = request.getParameter("cmd");
		System.out.println("cmd=" + cmd);
		
		
		String id_ = request.getParameter("id");
		int id = -1;
		try {
			id = Integer.parseInt(id_);
		} catch (Exception e) {
		}
		
		oldBean = customerDao.find(id);
		if (oldBean !=null)
			current = oldBean;

		// String name = request.getParameter("name");
		// String address = request.getParameter("address");

		if ("editer".equals(cmd) || cmd == null)
			newBean = current;
		else if ("suivant".equals(cmd))
			newBean = customerDao.next(current);
		else if ("pr�c�dent".equals(cmd))
			newBean = customerDao.prior(current);
		else if ("dupliquer".equals(cmd))
			newBean = customerDao.clone(current);
		else if ("supprimer".equals(cmd))
			newBean = customerDao.delete(current);
		else if ("ajouter".equals(cmd))
			newBean = customerDao.create();
		else if ("annuler".equals(cmd))
			newBean = current;
		else if ("modifier".equals(cmd)){
			current = formValue(request);
			if (current != null)
			   newBean = customerDao.merge(current);
		}
		else if ("foobar".equals(cmd))
			newBean = foobar(current);

		request.getSession().setAttribute(TestCustomer.CUSTOMER_CURRENT, newBean);

		String customerJSP = "/customers.jsp";
		RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(customerJSP);
		dispatcher.forward(request, response);
	}

	private Customer foobar(Customer current) {
		// TODO Auto-generated method stub
		return null;
	}

}
