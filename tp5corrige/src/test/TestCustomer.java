package test;

import java.util.ArrayList;
import java.util.List;

import bank.Customer;

public class TestCustomer {
	
	
	public static final String CUSTOMER_LIST = "custs";

	public static final String CUSTOMER_CURRENT = "cust";

	public static List<Customer> mesCustomers=new ArrayList<Customer>();
	
	public static void main(String[]a){
		
		int i=0;
		
		Customer c1= new Customer();
		c1.setName("Dupont");
		c1.setAddress("10, rue des roses");
		c1.setPhone("06.24.36.45.87");
		c1.setZipCode("30000");
		c1.setId(++i);
		mesCustomers.add(c1);
		Customer c2 = new Customer();
		c2.setName("Durand");
		c2.setAddress("20 rue des violettes");
		c2.setPhone("01.15.36.45.99");
		c2.setZipCode("34000");
		c2.setId(++i);
		mesCustomers.add(c2);
		Customer c3 = new Customer();
		c3.setName("Dujnou");
		c3.setAddress("30 rue des tilleuls");
		c3.setPhone("03.17.36.45.07");
		c3.setZipCode("81000");
		c3.setId(++i);
		mesCustomers.add(c3);

		for (Customer c:mesCustomers)
			System.out.println(c.getId()+" "+c.getName());
		
		
		
	}

}
