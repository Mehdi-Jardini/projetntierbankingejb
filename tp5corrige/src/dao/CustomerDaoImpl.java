package dao;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;



import bank.Customer;
import test.TestCustomer;
import utils.Paging;

public class CustomerDaoImpl implements CustomerDao, Serializable {


	private static final long serialVersionUID = 572652622633344240L;
	private int nextInt=10; //for test only

	/*
	private static CustomerDao instance = null;//new CustomerDaoImpl();

	
	public static CustomerDao getInstance(){
		if (instance == null)
			 instance =  new CustomerDaoImpl();
		return instance;
	}
	*/
	public CustomerDaoImpl() {
		super();
		System.out.println("creating CustomerDaoImpl");
	}



	public Customer add(Customer c) {
		addCustomer(c);
		return c;
	}


	private Customer copyCustomer(Customer c) {
		Customer newc = new Customer();
		if (c.getAddress() != null)
			newc.setAddress(c.getAddress());
		if (c.getName() != null)
			newc.setName(c.getName());
	//	if (c.getForName() != null)
	//		newc.setForName(c.getForName());
		// if (b.getZipCode() != 0)
		newc.setZipCode(c.getZipCode());
		// copier aussi les comptes
		return newc;
	}

	public Customer clone(Customer c) {
		Customer cloned = copyCustomer(c);
		add(cloned);
		return cloned;
	}
	

	
	public Customer dodelete(List<Customer> customers, Customer c) {
		Customer result = Paging.prior(customers, c);
		if (result==null)
			result = Paging.next(customers, c);
		customers.remove(c);
	    return result;
	}

	public Customer delete(Customer c) {
		return dodelete(getList(), c);
	}


	public Customer find(int id) {
		for (Iterator<Customer> i = getList().iterator(); i.hasNext();) {
			Customer current = i.next();
			if (current.getId() == id)
				return current;
		}
		return null;
	}
	

	public Customer first() {
		if (getList().iterator().hasNext())
			return getList().iterator().next();
		else
			return null;
	}


	public Customer last() {
		return getList().get(getList().size() - 1);
	}



	public Customer save(Customer c) {
		Customer result = find(c.getId());
		if (result != null) {
			if (c.getAddress() != null)
				result.setAddress(c.getAddress());
			if (c.getName() != null)
				result.setName(c.getName());
			//if (c.getForName() != null)
			//	result.setForName(c.getForName());
			// if (b.getZipCode() != 0)
			result.setZipCode(c.getZipCode());
			// comptes ??
		} else
			throw new RuntimeException("la modification de " + c + " a �chou�");
		return result;
	}

	public Customer create() {
		Customer c = new Customer();
		c.setId(nextInt++);
		c.setName("nouveau Customer " + c.getId());
		add(c);
		return c;
	}
	
	public Customer next(Customer c) {
		return Paging.next(getList(), c);
	}

	public Customer prior(Customer c) {
		return Paging.prior(getList(), c);
	}

	public Collection<Customer> reverseList() {
		List<Customer> rev = new ArrayList<Customer>();
		for (Iterator<Customer> i = getList().iterator(); i.hasNext();) {
			Customer c = i.next();
			rev.add(c);	
		}
		Collections.reverse(rev);
		return rev;
	}

	public boolean isEmpty() {
		return getList().size()==0;
	}

	public List getItems() {
		return getList();
	}

	public Customer merge(Customer c) {
		return save(c);
	}

	private void addCustomer(Customer c) {
		TestCustomer.mesCustomers.add(c);
	}


	@Override
	public List<Customer> getList() {
		return TestCustomer.mesCustomers;
	}


/*
	@Override
	public List<Customer> populate() {
		// TODO Auto-generated method stub
		return null;
	}
*/

	

}
