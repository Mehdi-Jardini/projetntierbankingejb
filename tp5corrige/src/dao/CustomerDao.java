package dao;

import java.util.List;

import bank.Customer;


public interface CustomerDao {
	public List<Customer> getList();
	public Customer find(int id);
	public Customer delete(Customer c);
	public Customer clone(Customer c);
	public Customer first();
	public Customer last();
	public Customer prior(Customer c);
	public Customer next(Customer c);
	public Customer create();
	public Customer merge(Customer c);
}
