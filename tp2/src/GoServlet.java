

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class GoServlet
 */
public class GoServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		
		request.getSession().setAttribute("k1", "bonjour depuis la servlet");

		String helloJSP = "/hello.jsp";

		RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(helloJSP);

		dispatcher.forward(request,response);	
		
	
	}

}
