package bank;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import demo.Customer;
import demo.TestCustomer;

/**
 * Servlet implementation class CustomerServlet
 */
@WebServlet("/customers.do")
public class CustomerServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see Servlet#init(ServletConfig)
	 */
	public void init(ServletConfig config) throws ServletException {
		super.init(config);
		TestCustomer.main(null);
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		List<Customer> model = TestCustomer.mesCustomers;
		request.getSession().setAttribute(TestCustomer.CUSTOMER_LIST, model);
		String id = request.getParameter("id");
		String name = request.getParameter("name");
		String address = request.getParameter("address");
		String cmd = request.getParameter("cmd");
		if ("supprimer".equals(cmd));
		if ("suivant".equals(cmd));
		int id_=0;
		try {
			id_ = Integer.parseInt(id);
		} catch (Exception e) {	
		}  
		Customer current = model.get(0);
		for (Customer c:model)
			if (c.getId() ==id_)
				current= c;
		request.getSession().setAttribute(TestCustomer.CUSTOMER_CURRENT, current);
        if ("modifier".equals(cmd)){
			current.setName(name);
			current.setAddress(address);
		}
		String customerJSP = "/customers.jsp";
		RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(customerJSP);
		dispatcher.forward(request,response);	
	}

}
