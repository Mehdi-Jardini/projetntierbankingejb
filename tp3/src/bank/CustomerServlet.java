package bank;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;



import demo.TestCustomer;

/**
 * Servlet implementation class CustomerServlet
 */
@WebServlet("/customers.do")
public class CustomerServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see Servlet#init(ServletConfig)
	 */
	public void init(ServletConfig config) throws ServletException {
		super.init(config);
		TestCustomer.main(null);
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		List model = TestCustomer.mesCustomers;
		request.getSession().setAttribute(TestCustomer.CUSTOMER_LIST, model);
		request.getSession().setAttribute(TestCustomer.CUSTOMER_CURRENT, model.get(0));
		
		
		String customerJSP = "/customers.jsp";

		RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(customerJSP);

		dispatcher.forward(request,response);	
	
	}

}
