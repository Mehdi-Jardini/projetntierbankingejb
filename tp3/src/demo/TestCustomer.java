package demo;

import java.util.ArrayList;
import java.util.List;

public class TestCustomer {
	
	
	public static final String CUSTOMER_LIST = "custs";

	public static final String CUSTOMER_CURRENT = "cust";

	public static List<Customer> mesCustomers=new ArrayList<Customer>();
	
	public static void main(String[]a){
		
		int i=0;
		
		Customer c1= new Customer();
		c1.setName("Dupont");
		c1.setId(++i);
		mesCustomers.add(c1);
		Customer c2 = new Customer();
		c2.setName("Durand");
		c2.setId(++i);
		mesCustomers.add(c2);
		Customer c3 = new Customer();
		c3.setName("Dujnou");
		c3.setId(++i);
		mesCustomers.add(c3);

		for (Customer c:mesCustomers)
			System.out.println(c.getId()+" "+c.getName());
		
		
		
	}

}
